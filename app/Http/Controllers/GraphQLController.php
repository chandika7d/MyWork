<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use GraphQL;

class GraphQLController extends Controller
{
    public function Query(Request $request)
    {
        $query = Input::get('query');
        if(!$query){
            exit();
        }
        $result = GraphQl::query($query);
        return \Response::json($result,200);
    }

    public function Mutation(Request $request)
    {
        $query = Input::get('query');
        if(!$query){
            exit();
        }
        $result = GraphQl::mutation($query);
        return \Response::json($result,200);
    }
}
