<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
    protected $table = "hari";

    public function JadwalPelajaran()
    {
        return $this->hasMany('App\JadwalPelajaran','idhari');
    }
}
