<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class HariType extends BaseType
{
    protected $attributes = [
        'name' => 'HariType',
        'description' => 'A type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::Int()),
            ],
            'namahari' => [
                'type' => Type::String(),
            ],
            'jadwalpelajaran' => [
                'args' => [
                    'jampelajaran' => [
                        'type' => Type::Int(),
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('JadwalPelajaranType')),
            ],
            'matapelajaran' => [
                'args' => [
                    'nmmatapelajaran' => [
                        'type' => Type::String(),
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('MataPelajaranType')),
            ],
        ];
    }
}
