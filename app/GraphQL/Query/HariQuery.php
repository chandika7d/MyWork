<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Hari;

class HariQuery extends Query
{
    protected $attributes = [
        'name' => 'HariQuery',
        'description' => 'A query'
    ];

    public function type()
    {
        return Type::listOf(GraphQl::type('HariType'));
    }

    public function args()
    {
        return [
            'id' => [
                'type' => Type::Int(),
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        if(isset($args['id'])){
            return Hari::where('id',$args['id'])->get();
        }else{
            return Hari::all();
        }
    }
}
