<?php

use Illuminate\Database\Seeder;

class hari extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hari')->insert([
            [
                'namahari' => "SENIN",
            ],
            [
                'namahari' => "SELASA",
            ],
            [
                'namahari' => "RABU",
            ],
            [
                'namahari' => "KAMIS",
            ],
            [
                'namahari' => "JUMAT",
            ],
        ]);
    }
}
