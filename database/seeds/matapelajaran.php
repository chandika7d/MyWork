<?php

use Illuminate\Database\Seeder;

class matapelajaran extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matapelajaran')->insert([
            [
                'nmmatapelajaran' => "KERJA PROYEK",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "MATEMATIKA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "SENI BUDAYA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PENDIDIKAN KEWARGANEGARAAN",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "BASIS DATA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PEMPROGRAMAN PERANGKAT BERGERAK",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "BAHASA INGGRIS",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PEMPROGRAMAN WEB DINAMIS",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PEMPROGRAMAN BERORIENTASI OBJEK",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "KEWIRAUSAHAAN",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "BAHASA INDONESIA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PENDIDIKAN AGAMA ISLAM",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "OLAHRAGA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "PEMPROGRAMAN GRAFIK",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "SEJARAH",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "BAHASA SUNDA",
                'namaguru' => "",
                'keterangan' => "",
            ],
            [
                'nmmatapelajaran' => "ADMINISTRASI BASIS DATA",
                'namaguru' => "",
                'keterangan' => "",
            ],
        ]);
    }
}
